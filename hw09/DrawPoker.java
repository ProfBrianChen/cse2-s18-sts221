//CSE2-SP18 Seth Slain hw09 array work
//DrawPoker.java work: 


//this program will generate a random deck of 52 cards and create hands for each of two people
//and then determines who will win based on the rules of poker and the descending order of cards
//

import java.util.Random; //import random class 
public class DrawPoker{//start of DrawPoker class
  
  public static void main(String[] args){//start of main method of the program
    //these two varaibles control the 'score' basically will be assigned to a higher value for a better hand 
    int score1 = 0;//score of player one 
    int score2 = 0;//score of player two 
    //scoring: high card = 0, pair = 1, three of a kind = 2, flush = 3, full house = 4
    int [] card = new int[52];//declar and allocate the card array 
    for(int i = 0; i<52; i++){//initalzie the card array with values from 0 to 51
      card[i] = i;
    }
    int[] card_2 = shuffle(card);//declare and assign the value of a new array to the shuffeled version of the first array 
    String use = print_out(card_2);//use the print_out method to turn the mixed up array into a string
    System.out.println(use);//print out this string 
    
    
    
    //throughout making this program I would test the method by manually entering data below
    int[] hand1 = new int[5];//declare and allocate the hand arrays 
    int[] hand2 = new int[5];
    for(int i = 0; i<10;i++){//initalize the two arrays based on the values of the scrambleed up array 
      if(i%2==0){//hand1 
        hand1[i/2] = card_2[i];
      }
      if(i%2!=0){//hand2
        hand2[(i-1)/2] = card_2[i];
      }
    }
   String hand1_print = "{";//do not send to printout becasue that is meant for a specific array size 
    for(int j = 0;j<5;j++){
      hand1_print+=""+hand1[j]+", ";
      
    }
    hand1_print +="}";
    String name_1 = card_name(hand1);//use the card_name method to create a string that will show the user what
    //actual cards they have 
    System.out.println("Player one:");
    System.out.println(name_1);
    
    String name_2 = card_name(hand2);
    System.out.println("Player two:");
    System.out.println(name_2);
    
    
    
    String hand2_print = "{";
    for(int k = 0;k<5;k++){
      hand2_print+=""+hand2[k]+", ";
      
    }
    hand2_print +="}";
    System.out.println("Hand 1: "+ hand1_print);
    System.out.println("Hand 2: "+ hand2_print);
    
    boolean pair_1;//test variables assigned to a boolean state based on the return from the methods
    boolean pair_2;
    
    
    
    //CODE FOR PALYER 1
    
    
    boolean flush1;
    flush1 = flush(hand1);//will test if hand 1 has a flush with the flush method
    //keep these in the scope of the main method for checking which hand is best 
    boolean full1 =false;
    boolean three_kind_1;

    pair_1 = pair(hand1);//will check if there is a pair
    //you have to distinguish between a pair and three of a kind if they occur outside of each other making a full house
    if(pair_1 == true){//evaluate if there is a pair 
      
      //evaluate within here because you know two must already be equal
      three_kind_1 = three_kind(hand1);
      if(three_kind_1==true){
        //will do the evaluation for a full house here 
        
        full1 = full_house(hand1);
        if(full1 == true){//full house is better than all other options
          System.out.println("Player one has a full house");
          score1=4;//assign score value as described above
          
        }
        else{
          if(flush1 == true){
            System.out.println("Player one has a flush");
            score1 =3;
          }
          else{
          System.out.println("Player one has three of a kind");
            score1=2;
          }
        }
      }
      else{ //put this statement here in case of overlap
        //overlap as in three of a kind will have three two pairs in it
        //always test for a flush because it is different then the anatomy of the pairs
        if(flush1 == true){
          System.out.println("Player one has a flush");
          score1 = 3;
        }
        else{
           System.out.println("Player one has a pair");
          score1 = 1;
        }
      }
    }
    else{
      if(flush1 == true){
          System.out.println("Player one has a flush");
          score1 = 3;
          
        }
      else{
        System.out.println("Player one has a high card");
        score1 = 0;
      }
  }
    
    
    
    
    
    //CODE FOR PLAYER 2 
    //exact same code as above; I shoudl have maybe written a method 
    
    boolean full2 = false;
    boolean three_kind_2;
    boolean flush2;
    flush2 = flush(hand2);
    //we will now evaluate the pair states for the second pair
    pair_2 = pair(hand2);
    if(pair_2 == true){
      
      //evaluate within here because you know two must already be equal
      three_kind_2 = three_kind(hand2);
      if(three_kind_2 == true){
        //within this, call the method for the full house
 
        full2 = full_house(hand2);
        if(full2 == true){
          System.out.println("Player two has a full house!");
          score2 =4;
        }
        else{
          if(flush2 == true){
            System.out.println("Player two has a flush");
            score2 = 3;
          }
          else{
           System.out.println("Player two has three of a kind");
            score2 = 2;
          }
        }
      }
      else{
        if(flush2 == true){
          System.out.println("Player two has a flush");
          score2 = 3;
        }
        else{
          System.out.println("Player two has a pair");
          score2 = 1;
        }
      }
    }
    else{
      if(flush2== true){
        System.out.println("Player two has a flush");
        score2 = 3;
      }
      else{
       System.out.println("Player two hasa high card");
        score2 =0;
      }
    }
    if(score1>score2){
      System.out.println("Player one wins!");
    }
    if(score2>score1){
      System.out.println("Player two wins!");
      
    }
    if(score1==score2){
      System.out.println("Both players have the same score");
    }
    
    
  }
  
  
  //this method will return the entire hand of a player in a string
  public static String card_name(int[] hand){
    String full_name ="{ ";
    for(int i=0;i<5;i++){
      
      //this next part will determine the card based on the modulus position 
      if(hand[i]%13 ==0){
        full_name+="ace of";
      }
      if(hand[i]%13 ==1){
        full_name+="two of";
      }
       if(hand[i]%13 ==2){
        full_name+="three of";
      }
      if(hand[i]%13 ==3){
        full_name+="four of";
      }
      if(hand[i]%13 ==4){
        full_name+="five of";
      }
      if(hand[i]%13 ==5){
        full_name+="six of";
      }
       if(hand[i]%13 ==6){
        full_name+="seven of";
      }
      if(hand[i]%13 ==7){
        full_name+="eight of";
      }
      if(hand[i]%13 ==8){
        full_name+="nine of";
      }
      if(hand[i]%13 ==9){
        full_name+="ten of";
      }
       if(hand[i]%13 ==10){
        full_name+="jack of";
      }
      if(hand[i]%13 ==11){
        full_name+="queen of";
      }
      if(hand[i]%13 ==12){
        full_name+="king of";
      }
      //first part will determine the suit based on which quartile 
      if((int) (hand[i]/13)==0){
        full_name+=" diamonds, ";
      }
      if((int) (hand[i]/13)==1){
        full_name+=" clubs, ";
      }
      if((int) (hand[i]/13)==2){
        full_name+=" hearts, ";
      }
      if((int) (hand[i]/13)==3){
        full_name+=" spades, ";
      }
    }
    full_name+="}";
    return full_name;//returns string type
  }
  
  
  //method will check for a full house off of looking for the number of pair mis matches
  public static boolean full_house(int[] hand){
    //basically this will check the number of pairs of nubmers that differ
    //this value should be 6 for a full house 
    int counter =0;
    for(int i =0 ;i<5;i++){
      for(int j =i;j<5;j++){
        if(hand[i]!=hand[j] && i!=j){
          counter++;
        }
      }      
    }
    
    if(counter == 6){//will have six pairs that have different values only in a full house
      return true;
    }
    return false;
  }
  
  //this will check to see if all of the 5 cards in a hand are of the same suit
  public static boolean flush (int[] hand){
    //not the most simplifed check but it works
   if((int)(hand[0]/13) == (int)(hand[1]/13) && (int)(hand[1]/13)== (int)(hand[2]/13) && (int)(hand[2]/13)== (int)(hand[3]/13) && (int)(hand[3]/13)== (int)(hand[4]/13)){
     return true;
   }
    return false;
  }
  
  //will check if there are three of the same kind of card
  public static boolean three_kind(int[] hand){
    int card_1 = hand[0]%13;
    int card_2 = hand[1]%13;
    int card_3 = hand[2]%13;
    int card_4 = hand[3]%13;
    int card_5 = hand[4]%13;
    //apply this to see that two of them are 
    for(int i =0;i<5;i++){
      for(int j=0;j<5;j++){
        if(hand[i]==hand[j] && i!=j){//i!=j is necessary to make sure you do not compare a number to itself 
          for(int k =0; k<5; k++){
            if(hand[i]==hand[k]&& i!=k &&j!=k){
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  
  
  //this will check to see if there is a pair at all; will check all ten possible pairs 
  public static boolean pair(int[] hand){
    int card_1 = hand[0]%13;
    int card_2 = hand[1]%13;
    int card_3 = hand[2]%13;
    int card_4 = hand[3]%13;
    int card_5 = hand[4]%13;
    if(card_1 == card_2|| card_1 == card_3 || card_1 == card_4 || card_1== card_5||card_2==card_3|| card_2 == card_4|| card_2 == card_5|| card_3 == card_4 || card_3 == card_5|| card_4 == card_5){
      return true;
    }
    return false;
  }
  
  //this will convert any array into string with the legnth of 52
  public static String print_out(int[] input){
    String print = "{";
    for(int i = 0;i<52;i++){
      print+=""+input[i]+", ";
      
    }
    print +="}";
    return print;
  }
  
  //like in hw08 the problem comes in trying to assign to a new array but should just manipulate 
  //an already existing one and 'use' should be a palceholder indicator 
  public static int[] shuffle(int[] input ){
    int[] use = new int[52];
    for(int j=0;j<52;j++){
      use[j] = 0;
    }
    int val;    
    
    Random rand = new Random();//declare and consturct the randnom nubmer generator rand
    for(int i = 0; i<52; i++){
      val = rand.nextInt(52);
      if(use[val]==0){
        input[i] = val;
      }
      if(use[val] ==-1){
        i--;
        continue;
      }
      use[val] = -1;
     
    }
    
    
    return input;//will return the final shuffled array 
  }
  
}




