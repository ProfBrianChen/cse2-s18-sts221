//CSE2-SP18 Seth Slavin
//hw10 RobotCity.java
//this assignment will basically allow you to randomly make a city block with varying dimensions
//it will then fill that city with population values from 100 to 999 and then have a robotic invasion 
//of a random number of invaders appear at random spots. Each itteratino they will invade the block to the east
//for five times as new invaders come. This all will be displayed for the user to see.

import java.util.Random;//import the random class
public class RobotCity{//class of program
  public static void main(String[] args){//main method of the program 
      Random rand = new Random();//declear and construct a random value choice
      int [][] city = buildCity();//declare the city 2D array that the buildCity method makes
 
      //process in this loop is to invade, display, update and then repeat 5 times 
     System.out.println("Invasion has begun!");
      for(int i =0;i<5;i++){//will go through 5 times 
        //I think I made the possible invading value too large so what about 10?
      
        int k = Math.abs((rand.nextInt()%(10))+1);//will chose a random value between 1 and 10 invaders 
        System.out.println("k value: "+k); //display of number of invaders 
        System.out.println("Invade: ");
        invade(city,k);//will call for the given nubmer of invaders to be placed at random places 
        System.out.println("update: ");
        update(city);//will update the map by putting all of the current robots in the next block over 
      }
  }
  
  
  //methods outside of main: 
  
  public static void update(int[][] map2){//update method
    System.out.println();
    //basically have to make all members of an array to the right of a negative value turn negative
    //but it looks like update only is applied once. so they will move to the left
    for(int i = 0; i<map2.length;i++){//will check over the whole map
      int check = 1;//this variable is used for when you want to only move them over by 1 but have 
      //multiple negatives spread out in a row with positive spaces in between
      for(int j=0;j<map2[0].length;j++){//will itterate over the member array length
        
        if(map2[i][j]<0){//condition that it found a negatvie one
          //must check this for each negative one!
          if(j+1<map2[0].length){
            //need to get this to only update it by multiplying the one over by negative one 
            if(map2[i][j+1]<0 ){
              check =1;//will make it 1 so that when it gets to the next possible negative one it will be able to multiply the next one over correctly 
               continue;
            }
            if(map2[i][j+1]>0 &&check == 1){
               map2[i][j+1]*=-1;
              check = 0; //will set this value to zero so that the whole row to the right will not become negative when it should not be 
            }
          }
        }
      }
    }
    display(map2);//final display 
    
  }
  
  public static void invade(int[][] map1,int num){//method sued to randomly place new invaders down 
    Random rand = new Random();
    //have to make the robots invade but only invade once! It will turn the original value negative
    int[][] attack_spots =new int[map1.length][map1[0].length];//used to show where the attacks will be 
    //will it only print out values of zero?
    //maybe we have to articially assign those initally 
    int counter =0;
    //this below will check that there are enough postive spaces left 
    for(int j =0; j<map1.length;j++){
      for(int k =0; k<map1[0].length;k++){
        attack_spots[j][k]=1;
        //I am essentailly trying to kill two birds with one stone with this loop here
        if(map1[j][k]>0){
           counter++; //the number of nonnegative spots left             
        }
      }
    }
    //this function below will count off to make sure that for the multiple itteratinos that 
    //there will be enough spots left, otherwsie the program will stall
    //You can actually just include this in the loop above 
    for(int i=0; i<num&& counter>0;i++){
      
      int y_spot = Math.abs(rand.nextInt()%(map1.length));
      int x_spot = Math.abs(rand.nextInt()%(map1[0].length));
      if(attack_spots[y_spot][x_spot]==1&&map1[y_spot][x_spot]>0){
        attack_spots[y_spot][x_spot]=-1;
        counter--;
        continue;
      }
      i--;
    }
    /* I used to print there out for my use but I got rid of it 
    System.out.println("attack spots:");
    display(attack_spots);
    */
    for(int l =0; l<map1.length;l++){
      for(int m = 0; m<map1[0].length;m++){
        map1[l][m]= attack_spots[l][m]*map1[l][m];//bascially muliptlies given spots by -1 
      }
    }
    display(map1);
  }
  
  //this is the method only used once to build a full city 
  public static int[][] buildCity(){
    Random rand = new Random();
    int east_west=(rand.nextInt()%5)+10;//length of each array member 
    int north_south =(rand.nextInt()%5)+10;//length of array
    System.out.println("East-west: "+east_west);
    System.out.println("north-south: "+north_south);
    int [][] city = new int[north_south][east_west];
    //initalziation:
    for(int i = 0; i<city.length; i++){
      for(int j = 0; j<city[0].length; j++){
        //values in city blocks must go between 100 and 999
        city[i][j]=Math.abs(Math.abs((rand.nextInt()%899))+100);//must confine values from 100 to 999
      }
    }
    display(city);

    return city;
  }
  //commonly used printout method to show the updated map in full
  public static void display(int[][] map){
    for(int i = 0; i<map.length;i++){
      for(int j = 0; j<map[0].length;j++){
        System.out.printf(" %4d",map[i][j]);
        
      }
      System.out.println();
    }
  }
}