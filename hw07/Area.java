//Seth Slavin CSE 2- SP 18 hw07
//this program will test knowledge of methods and operations with them 
//The program will provide a prompt for the user to input one of the three possible shapes and will use seperate methods
//to obtain the proper dimensions with the correct data type and display the final area 
import java.util.Scanner; //import the Scanner class

public class Area{ //start the class Area 
    
  public static void main (String [] args){ //start the main method of the program 
    Scanner S;//decalre the scanner 
    S = new Scanner(System.in); //initalize the scanner with the consturctor operator 'new'
    
    
    System.out.println("Welcome!"); //inital welcome statements
    System.out.println("Please type in one of the three following shapes to find the area of: ");
    System.out.println("rectangle, triangle, or circle");
    
    boolean type_I = S.hasNextInt(); //test to see if input is a non string data type 
    boolean type_D = S.hasNextDouble();
    String junk; //used for washing away unwanted input 
    while(type_I == true || type_D == true){ //loop executes until the correct data type is entered 
      System.out.println("Please enter a string value:");
      junk = S.next();
      type_D = S.hasNextDouble();
      type_I = S.hasNextInt();
    }
    
    String rect = "rectangle";// used to just condense code slightly 
    String tri = "triangle";
    String cir = "circle";
    
    String shape =S.next(); //stores the input in the shape variable 
    
    while(shape.equals(rect) == false && shape.equals(tri) == false && shape.equals(cir) == false  ){ //this loop will make sure that it is one of the three acceptable possible inputs 
      System.out.println("Please enter a string with the proper name:");
      
      type_D = S.hasNextDouble();
      type_I = S.hasNextInt();
      while(type_I == true || type_D == true){ //this is used to make sure that another input will not evaluate due to the wrong type
        System.out.println("Please enter a string value:");
        junk = S.next();
        type_D = S.hasNextDouble();
        type_I = S.hasNextInt();
      }
      shape = S.next();
    }
    
    System.out.println("Your shape of choice: " + shape); //basic display to the user 
    System.out.println("");
    System.out.println("You will now be asked for the dimensions"); //now moving into the user prompt for input 
    if(shape.equals("rectangle")==true){ 
      rectangle(); //method call for the rectangle 
    }
    else if(shape.equals("triangle")==true){
      triangle(); //method call for the triangle 
    }
    else{
      circle(); //method call for the circle 
    }
    //once one of the three respective methods have been operated on, there will be a final system print out
    //which is END to alert the user of the end of the program.
   System.out.println("END"); 
  }
  
  //this is the method that makes sure that the input is correct
 
  
  public static Double obtain(double val){ //this is used a lot throughout the evaluation from the rectangle ,triangle, and circle methods
    Scanner S;
    S = new Scanner(System.in);
    String junk;
    boolean check = S.hasNextDouble();
    while(check == false){ //makes sure it is a double 
      System.out.println("Please enter a double value");
      junk=S.next();
      check = S.hasNextDouble();
    }
    val = S.nextDouble();
    while(val<=0){ //since we are working with lengths we have to make sure they are positive and non zero 
      System.out.println("Please enter a positive double");
      check = S.hasNextDouble();
      while(check == false){
        System.out.println("Please enter a double value");
        junk=S.next();
      }
      val = S.nextDouble();
    }
    return val; // will return the value for oepration in the next few methods (pass by value) 
  }
  
  
  public static void rectangle(){ //rectangle method 
    Scanner S; //scanner must be redecalred because it is outside of scope of the main method 
    S = new Scanner(System.in);
    String junk;
    System.out.println("What height would you like? (enter an double)");
    boolean check = false;
    double height =0.0; //set values to start so that the program knows some final value will be displayed 
    double width = 0.0;
   
    height = obtain(height); //passed essentially a pointless variable since the height will be asked for in the obtain method
    
    System.out.println("What width would you like? (enter a double)");
    
    width = obtain(width); //same process 
    
    System.out.println("Width: " + width); //print out for the user to see 
    System.out.println("Height: " + height );
    
    double area;
    area = width* height; //area calculation and final printout 
    System.out.println("The area of the rectangle will be " + area + " square units");
  }
  
  
  
  
  public static void triangle(){ //method for the triangle evaluation 
    Scanner S; //same with the Scanner 
    S = new Scanner(System.in);
    String junk;
    System.out.println("Please enter the height as a double");
    double height = 0.0;
    double width = 0.0;
    //this method is essentailly the same as for the rectangle but the area must be divided by two since it is 
    //a triangle 
    height = obtain(height); 
    
    System.out.println("Please enter the  width of the base perpendicular to the height used ");
    width = obtain(width);
    
    System.out.println("Height: " + height);
    System.out.println("Width: " + width);
    
    double area= height*width/2;
    System.out.println("The area of the triangle is "+ area + " sqaure units");
    
  }
  
  
  
  
  public static void circle(){ //method for the circle will be less because it needs only the radius dimension 
    Scanner S;
    S = new Scanner(System.in);
    String junk;
    System.out.println("Please enter the radius of the circle");
    double radius = 0.0;
    radius = obtain(radius);
    
    System.out.println("Radius: " + radius);
    
    final double PI = 3.14159265; //a fianl constant must be declared to reprenst PI
    double area = PI * (radius*radius); //calculation and final printout 
    System.out.println("The area of the circle will be " + area + " square units");
    
  }
}