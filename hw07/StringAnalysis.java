//Seth Slavin CSE2 SP18 hw07 program 2
//this program will work on string analysis
//it will ask for any string to be inputted and will analyze the different components of the string and tell you what each of the individual characters are and whether overall it was composed 
//of just letters or not 
import java.util.Scanner; //import the scanner method 

public class StringAnalysis{ //start the class StringAnalysis
  
  public static void main(String[] args){ //begin the main method of the program 
    Scanner S; //decalre the scanner  S
    S = new Scanner(System.in); //initalize the Scanner with the constructor operator 'new'
    
    System.out.println("Please enter a single string (no spaces) for being analyzed"); //prompt statements for the user 
    System.out.println("numbers, letters, and characters can be used");
    String input = S.next(); //no need to check this because all input will be accpetable 
    System.out.println("Would you like to analyze the whole string?"); //prompt for analzying whole string or the first certain number of characters 
    System.out.println("Please type in Y for yes of N for No");
    boolean type_I = S.hasNextInt();
    boolean type_D = S.hasNextDouble();
    String junk;
    while( type_I == true || type_D == true){ //check for the correct data type input for Y or N 
      System.out.println("Please enter a string of Y or N");
      junk = S.next();
      type_D = S.hasNextDouble();
      type_I = S.hasNextInt();
    }
    String ans = S.next();
    while(ans.equals("Y")== false && ans.equals("N")== false ){ //will make sure that the string entered will be either Y or N 
      System.out.println("Please enter a string of Y or N");
      junk = S.next();
      type_D = S.hasNextDouble();
      type_I = S.hasNextInt();
      while( type_I == true || type_D == true){
        System.out.println("Please enter a string of Y or N");
        junk = S.next();
        type_D = S.hasNextDouble();
        type_I = S.hasNextInt();
      }
      ans = S.next();
    }
    
    
    //this function may actually be able to find the string length more readily
    //it is important to know the maximum length of the string being worked with 
    int i_max = input.length();
    System.out.println("Max string position: " + i_max); //basic printout for the user 
    boolean result =true;// since there is a final test of result it is important to give it a blank value here 
    
    
    if(ans.equals("Y") == true){ //will send the input string and obtain a true boolean back if it is all letters and false boolean if it is not 
      result = finder(input);
    }
    if(ans.equals("N") == true){ //this will prompt the user for the number of characters to be analyzed and assure that value is within an acceptable range 
      System.out.println("Please enter the number of characters to check within the string");
      type_I = S.hasNextInt();
      while(type_I == false){
        System.out.println("Please enter an integer value");
        junk = S.next();
        type_I = S.hasNextInt();
      }
      int position = S.nextInt();
      while(position <=0 || position > i_max){ //will make sure it is a positive integer less than or equal to the string length 
        System.out.println("Please enter a positive integer value within range");
        type_I = S.hasNextInt();
        while(type_I == false){
          System.out.println("Please enter an integer value");
          junk = S.next();
          type_I = S.hasNextInt();
        }
        position = S.nextInt();
      }
      result = finder(input, position); //this will pass by value the string and maximum desired output checks 
      
    }
    if(result==true){ //when either method is run if all characters are letters result will be true and will make the display below 
      System.out.println("The string is all characters");
      
    }
    if(result == false){
      System.out.println("The string is not all characters");
    }
  }
  
  //they both have to have the same name but will be differentiated by the number and type of inputs

  public static boolean finder(String line){ //this method below will work for when the whole string is being analyzed 
    int i_max = line.length();
    int test = 0;
    for(int i =0; i<i_max ; i++){ //will increase until the limit of the loop has been reached 
      char letter = line.charAt(i);
        if(Character.isUpperCase(letter) &&letter>='A' && letter<= 'Z' ){ //this will be a necessary test for capital letters to assure they 
          //are not counted as non letters 
          System.out.println("The character at position " + (i+1) + " is a letter ");
          continue;
        }
        if(letter>='a' && letter<= 'z'){ //will check to see if the character is between a and z and if it is then it will know it is a character! 
          System.out.println("The character at position " + (i+1) + " is a letter ");
          continue;
        }
        if(letter<'a' || letter >'z'){ //on other ends of the spectrum it will not know it is a letter and will let the user know of this 
          System.out.println("The character at position " + (i+1) + " is not a letter");
          test++; //even after just one time this is incremented this will show that the method will return false becuase not all of the characters are definitely letters 
          continue;
        }

    }
    if(test == 0){ //will return the true or false based on the operations outlined above 
      return true;
    }
    else{
      return false;
    }
  }
  
  //this is not for the number position but for the number of positions
  //this method will allow to check a certain number of charcters 
  //it will essentially function the same with the same return conditions and general outputs but just with a different range of characters to check 
  public static boolean finder(String line, int num){
    int test =0;
    for(int i =0; i<num; i++){
      char letter = line.charAt(i);
      
      if(Character.isUpperCase(letter) &&letter>='A' && letter<= 'Z' ){ //this will test for any capital letters
        //originally they would not have been read as letters but this allowed them to be 
          System.out.println("The character at position " + (i+1) + " is a letter ");
          continue;
        }
      if(letter>='a' && letter<= 'z'){
          System.out.println("The character at position " + (i+1) + " is a letter ");
          continue;
        }
        if(letter<'a' || letter >'z'){
          System.out.println("The character at position " + (i+1) + " is a non letter symbol");
          test++;
          continue;
        }
        
    }
    if(test == 0){
      return true;
    }
    else{
      return false;
    }
  }
}