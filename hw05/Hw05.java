//CSE2 - SP18 Seth Slavin
//Hw05 course selction import correction
//Reading in Loops
//This assignment will act as a check for correctly inputting certain data types into the program by prompting the user on information relating to a course they are taking. The program
//will check to see that if the data type entered for the given question is useful or not. If the data type should be integer and a double or string is entered it will run back through until 
//the user has inputted the correct type. At the end all of the data will be displayed in the same order in which the questions were asked

import java.util.Scanner; //import statement for the scanner 

public class Hw05{ //class Hw05 for the program 
  public static void main(String[] args){ //main method of the program 
    Scanner S; //declare a scanner S
    S = new Scanner(System.in); //initalize the Scanner S with the constructor operator 'new' 
    String junkWord; //This will be the word used to displace useless inputs that are not the correct data type 
    
    System.out.println("Welcome!");//these statements are the initial prompt screen for the user 
    System.out.println("If you enter more than one value on each line the program will only accept the first"); //This basically says that if you enter two integers it will report the first
    //one for the given prompt but transfer that second one to the next question if it is asking for an integer, so try not to input multiple values. I checked this by Professor Chen and he
    //said that we do not need to make a safegaurd against this yet 
    
    System.out.println("Please think of a course you are taking");// let the user know the program will be asking about a course 
    
    
    System.out.println("What is the CRN for the course? Enter as an integer ");
    boolean CRN_right = S.hasNextInt(); //this is the Scanner function that will check the data type of the inputted value. If it is integer we will get true, if not, then false 
    while(CRN_right == false ){ //while loop used to continue to prompt the user until the correct data type has been entered 
      junkWord  = S.next(); //this will store the previous input as a string junkword 
      System.out.println("Invalid type for the CRN; please enter an integer");
      CRN_right = S.hasNextInt(); //new prompt that will wait for the user input 
    }
    int CRN = S.nextInt(); // once the correct type has been inputted the previous tests with 'has' in them will not remove that data but now it will be stored into the variable CRN and will
    //be removed accordingly for the next sequence of statements 
    
    
    System.out.println("What is the department abbreviation?"); //prompt 
    //Below I used two varaibles to act as booleans because the string input is strange. If you use just 'hasNext' it will not allow you to just hit enter and not input anything. It will also 
    //read integers as strings but we want an actaul string here, so the best thing I saw to do was to check if the inputted type was a double or an integer and if it is not assume it to be 
    //a string and then store it as the abbreviation 
    boolean abr_right = S.hasNextInt(); //first test variable 
    boolean abr_right_doub = S.hasNextDouble(); //second test variable 
    while(abr_right == true || abr_right_doub == true){
      junkWord  = S.next();
      System.out.println("Invalid type for the abbreviation ; please enter a string ");
      abr_right = S.hasNextInt();//you must use both in the body of the while loop. Again, they will not remove inputs but just check their type
      abr_right_doub = S.hasNextDouble();
    }
    String abr = S.next(); //the final string is stored here 

    
    System.out.println("How many times do you meet a week?"); //prompt 
    boolean meet_total_r = S.hasNextInt(); //this segment of code is the same as the first while loop asking for the CRN
    while(meet_total_r == false){
      junkWord = S.next();
      System.out.println("Invalid type for the number of meetings; please enter an integer");
      meet_total_r = S.hasNextInt();
    }
    int meet_total = S.nextInt();
    

    
    System.out.println("What time does the class meet?");
    System.out.println("Use military time and omit the colon"); //this is important because if the user inputted 11:10 this would be read as a string. Manipulating this data could be done by 
    //possibly dividing by 100 and finding the fractional remainder to find minutes, but since this was not asked for I just have the user input an unbroken integer 
    boolean start_right = S.hasNextInt();
    while(start_right == false){
      junkWord = S.next();
      System.out.println("Invalid type for the start time; please enter an integer ");
      start_right = S.hasNextInt();
    }
    int start_time = S.nextInt();

    
    System.out.println("What is the instructor's name?"); //same code as when the abbreviation was requested 
    boolean name_right_i = S.hasNextInt();
    boolean name_right_d = S.hasNextDouble();
    while(name_right_i == true || name_right_d == true ){
      junkWord = S.next();
      System.out.println("Invalid type for the instructor name; please enter a string");
      name_right_i = S.hasNextInt();
      name_right_d = S.hasNextDouble();
    }
    String name_teacher = S.next();  

    
    System.out.println("How many students are in the class"); //same code for the integer inputs 
    boolean stud_num_right = S.hasNextInt();
    while(stud_num_right == false){
      junkWord = S.next();
      System.out.println("Invalid type for the number of students; please enter an integer");
      stud_num_right = S.hasNextInt();
    }
    String stud_num = S.next();

   
    //the statements below will print out all of the corresponding data that we obtianed above with the correct type labeled accrodingly and presented in the order in which they were asked
    System.out.println("CRN: " + CRN);
    System.out.println("Class Abbreviation: " + abr);
    System.out.println("Number of times the class meets per week: " + meet_total);
    System.out.println("start time: " + start_time);
    System.out.println("Instructor's name: " + name_teacher); 
    System.out.println("Number of Students: " + stud_num);
  }
}
