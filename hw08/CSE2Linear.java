//CSE2 - SP18 Seth Slavin
//hw08 program 1 CSE2Linear.java
//this program allows a user to input the final scores of 15 students in the CSE2 course in an increasing order. It then prompts the user to look for a certian value and
//tells the user if that value is a member of the set by using a binary search method. The array is then randomzied and they are asked to input antoher value to search for in the new array which then goes to the linear
//search method to test the values to see and then report back if it was found and in how many program itteratinos it took 



import java.util.Scanner; //import the scanner method
import java.util.Random; // improt the random method 
public class CSE2Linear{ //start of the calss of the program 
  public static void main(String[] args){ //start of the main method fo the program 
    Scanner look; //declear the scanner 'look'
    
    look = new Scanner(System.in); //initalize the scanner with the constructor operator 'new' 
    
    //inital prompt screen for the user 
    System.out.println("Please enter the final scores of 15 students in CSE2");
    System.out.println("make sure the next number is greater than or equal to the last ");
    System.out.println("and that all values fall in the 0 - 100 range");
    System.out.println("---------------------------------------------------------------");
    
    int [] grade = new int[15]; //declare and allocate the memory for the grade array 
    boolean test = true;
    String junk = "a";
    for(int i = 0; i <15;i++){ //this whole loop bascially checks to make sure the user inputs both the right type and value ranges and the correct number of numbers
      System.out.println("Please enter the score of student "+ (i +1) +":");
      test = look.hasNextInt();
      if(test == false ){
        System.out.println("ERROR: invalid data type:");
        junk = look.next();
        i--;
        continue;
      }
      grade[i] = look.nextInt();
      if(grade[i] <0 || grade[i]>100){ //checks if the value is negative or more than 100
        System.out.println("ERROR: value out of range");
        i--;
        continue;
      }
      if(i!=0 && grade[i-1] >grade[i]){ //checks to make sure they are sequnetially entered 
        System.out.println("ERROR: value not larger than the last");
        i--;
        continue;
      }
      
    }
    System.out.print("Student scores: {");//this whole statement here works to print out to the user the score in a single row matrix format 
    for(int j = 0; j<15;j++){
      System.out.print(grade[j]+", ");
    }
    System.out.println("}"); //closing brace on the printout
    System.out.println("You will now be asked for a a grade to be looked for");//prompt statement for grade searching 
   int iterations = binary(grade); //sends the array to the binary search method (binary becasue it compares one to the other )
    
    if(iterations <=15){//this reads the int return of binary search which was the number of itterations it took
     System.out.println("It took "+ iterations + " itterations");
    }
    
    //the code will now move into the random scrambeling and will then send it to the linear search through binary search
    //and see how many respective itterations it will take this time
    System.out.println("The array will now be randomized");
    
    grade = randomized(grade); // use an array return type even though it is not necessary since it is pass by reference and the value will be auomatically changed 
    System.out.println("You will now be asked for a new value to find"); //this will be the last prompt for user input to check to search for a new value 
    linear(grade);
  }
  
  
  
  
  
  //i orgianlly wrote this wrong before I knew what a binary search was and left that code there but added in the more effeicnet
  //actaul binary search code here 
  public static int binary(int[] grade2){ //this is the binary serach method used in the first non randomzied array set up 
    Scanner look; //scanner needs to be redeclared and initalzied 
    look = new Scanner(System.in);
    
    boolean test; 
    String junk = "a";
    boolean result = false;
    int val = 0;
    while(result == false){ //loop checks for correct value input and range 
      System.out.println("Please enter a positive integer between 0 and 100 to look for: ");
      test = look.hasNextInt();
      if(test == false){
        junk = look.next();
        continue;
      }
      val = look.nextInt();
      if(val < 0 || val >100){
        continue;
      }
      result = true;
    }
    
    
    int itterations = 0;
    //I want to do this like the lecture example that uses the start and stop variables
    boolean check = false;
    int start = 0;
    int stop = grade2.length-1;
    int mid = 0;
    while(check == false){
      mid = (stop-start)/2+start;
      itterations++;
      if(stop == start || itterations>5){
        if(val!=grade2[mid]){
          System.out.println("Value not found");
          check = true;
        }
      }
      if(val==grade2[mid]){
        System.out.println("Value found!");
        check =true;
      }
      if(val>grade2[mid]){
        start = mid+1; //adjust the value of where to start at aka lower bound 
        
      }
      if(val<grade2[mid]){
       stop = mid - 1; //adjust the value where to end aka the upper bounds 
      }
    }
    return itterations;
    //this code will now compare over given ranges where the value could lie
    //it must return the number of iterations
    //if it could not find it then it will have a return on int that will be read accordingly
   /* int itterations = 0;
    for(int i = 0; i<grade2.length; i++){
      if(val>grade2[i]){
          itterations++;
        if(i == grade2.length-1){
          System.out.println("No match found");
          break;
         }
        }
      if(val==grade2[i]){
        itterations++;
        System.out.println("Match found!");
        break;
        }
      if(val<grade2[i]){
         itterations++;
         System.out.println("No match found");
         break;
        }
      }
    
    return itterations;//will return the value of itterations used as discsused above 
    } */
  
  }
  
  
  //this function will randomize the array by swapping around the values 
  public static int[] randomized(int[] grade3){
    Random rand = new Random(); //need to construct a random function called rand 
    int val;
    int [] use = new int[15]; //make a new array that will initally have all values set to 0 
    int input;
    System.out.print("Your new data set: {");
    for(int i = 0; i <grade3.length; i++){ //this will be the random reassign function 
      val = rand.nextInt(15); //it outputs from 0-14 which is what we want
      input = grade3[val];
      use[i] = input; // will assign as long as the following conditions below are met 
      if(use[i] == -1){ //used as a value out of range to show that it has been used up
        i--;
        use[i] = 0;
        continue; 
      }
      System.out.print( use[i]+ ", "); //display in the array form for the user 
      grade3[val] = -1; // used to signify an already filled 'box' 
    }
    System.out.println("}");
    for(int j =0;j<use.length; j++){//will reassign the values to the first array used (not entriely necessary) 
      grade3[j] = use[j];
    }
    return grade3; //returns the array of interest 
    
  }
  
  
  //this array will help to see if a value is present in the new scrambeled array 
  public static void linear(int[] grade4){ // this is basically alwasy going to take more itteratinos because it is chekcing each value since there are no relationships among the places
    //of values and the actual values themselves 
    Scanner look;
    look = new Scanner(System.in);
    
    
    boolean result = false;//this block here is exactly the same as used in the binary method
    int val = 0;
    String junk = "a";
    boolean test;
    while(result == false){
      System.out.println("Please enter a positive integer between 0 and 100 to look for: ");
      test = look.hasNextInt();
      if(test == false){
        junk = look.next();
        continue;
      }
      val = look.nextInt();
      if(val < 0 || val >100){
        continue;
      }
      result = true;
    }
   
    int check = 0;
    for(int i=0; i < 15; i++){
      
      if( val ==grade4[i]){//for some odd reason you have to express this as a if else statement. With just the if statement it would not run properly
        check++;
        System.out.println("A match was found");
        break;
      }
      if(val!=grade4[i]){
        check++;
      }
      
    }
    if(check==15){
      System.out.println("No match was found in 15 itterations");//output if no match was found 
    }
    if(check!=15){
      System.out.println(check+ " itterations were used"); //since it is a void return type I do not have it send anything back up so I just use the print outs here 
    }
  }
 
  
}