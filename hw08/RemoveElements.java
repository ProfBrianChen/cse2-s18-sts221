//CSE2-SP18 Seth Slavin
//hw08 RemoveElements program
//this program will basically work to randomly generate an array of 10 integers and will allow the user first to decide which position will be eliminated and then what specific values
//should be removed and this will all be outputted to the user


import java.util.Scanner; // import the scanner class
import java.util.Random;//import the random class

public class RemoveElements{ //start of the class 
  public static void main(String [] arg){  //start of the main method of the program
	Scanner scan=new Scanner(System.in);//declare and initalize the scanner in one line 
    int num[]=new int[10];
    int newArray1[]; // I ended up not using these arrays because something of using different arrays was messing with the reustls, so to avoid these I did not use them 
    int newArray2[];
    int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
    
    //this is the first method we have to make on our own below 
  	num = randomInput(); // will make the array an output it 
    
    
  	String out = "The original array is:";
  	out += listArray(num); //this method will provide a string output for the array sicne you cannot just output an array in a good looking format 
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
    
    //this is the second method for us to make
  	num = delete(num,index);
    
    
  	String out1="The output array is ";
  	out1+=listArray(num); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
    
    //this will be the third and last array to make
  	num = remove(num,target);
    
    
  	String out2="The output array is ";
  	out2+=listArray(num); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
    
  }
 
	//this code below is the three methods that I wrote from scratch 
	
	//aside from this first method below 
	
  public static String listArray(int num[]){ //this is the listArray method that is used to turn the array into a pleasnat looking string 
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }

//random input array 
public static int [] randomInput(){
  Random rand = new Random(); //decalre rand   a random number searcher 

  int [] use = new int[10];
  for(int i = 0; i <10; i++){ //bascially just makes the random values and then return the array for the manipulation through the rest of the program 
    use[i] = rand.nextInt(10);
    
  }
  
  return use;
 }

  //this will be the delete array that will remove a given position 
public static int [] delete(int[] num2, int posi ){
  int [] num3 = new int[9];
  for(int i = 0; i<9;i++){
    
		if(i >= posi){
			num3[i]=num2[i+1];//bascially use when the i value is at the position to check for one above
			//when I did it before I was getting errors with not filling values
			continue;
			
		}
    num3[i] = num2[i];
  }
  return num3;
} 
  
  
  //this final method will be for deleting all of a specific target number in the array
  public static int[] remove(int[] num4, int target){
		
		//the comments below were from before i just used 'num' up top. I really want to know why using the variables he had made it a problem?
		
		
		//i initally tried this within this whole loop but the output was so strange and I could not figure out why. I commented out the code I used before
		//but will be using the uncommented code that is present here:
		/*for(int i = 0; i<num4.length; i++){
			if(num4[i] == target){
				num4[i] = 0;
			}
		}
		String out1="The output array is ";
  	out1+=listArray(num4); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
		return num4;
		*/
		
		
		
		
   int take_off = 0; //used to indicate how many of the target number there are 
    for(int i = 0; i<9; i++){
      if(num4[i] == target){
        take_off++;
      } 
    }
    //difficult to get it to make sure you checked throguh the whole loop the right number of times 
    int [] num5 = new int[9-take_off]; //create an array of this new length 
		for(int k = 0; k<9-take_off; k++){
			num5[k] = 0;
		}
		int diff = 0;//initalize a value of 0 that will be made larger as needed
		
    for(int j = 0; j< 9 - take_off;j++){//this is fairly complex in its operations but it basically will skip over the values and reassign the next one or if two are in a row of the target
			//it will skip to the third one after I did not put any safe guards in for three in a row but I could continue this process. A 1/1000 chance is one I am willing to take.
			
      if(num4[j+diff] == target){//if they are equal it checks then if the next one is the same as well 
        if(num4[j+1+diff] != target){
					num5[j] = num4[j+1+diff];
					diff++; //adds to the difference to indicate how many of the target number has been reached 
					continue;
				}
			  if(num4[j+1+diff] == target){
					num5[j]=num4[j+2+diff];
					diff++;//use two here because you skip two numbers 
					diff++;
					continue; //these prevent the assignemnt of the value 
				}
				
      }
			if(num4[j+diff] != target){//general assignemnt 
     	 num5[j] = num4[j+diff];
			}
    }
		
    return num5;//final return of the array back to the main method of the program for final print out 
		
  }
  
  
  
 
}