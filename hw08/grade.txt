Grade:  96/100

Comments:
A) Does the code compile?  How can any compiler errors be resolved?
Both programs compile correctly.
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
In CSE2, there is an error with the calculation of iterations only one the not scrabbled array. 
C) How can any runtime errors be resolved?
Compare the iterations for the unscrambled vs scrabbled calcuations.
D) What topics should the student study in order to avoid the errors they made in this homework?
E) Other comments:
Overall, nice job.