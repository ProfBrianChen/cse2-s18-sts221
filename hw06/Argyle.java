//CSE2-SP18 Seth Slavin
//Argyle.java
//this program will take input from the user and generate the desired argyle pattern. It will ask for the window height, window width, the side length of the diamond,the center width thickness
//the three character types and will generate an output that matches these desired specifications. 


import java.util.Scanner; //import the scanner method

public class Argyle{ //open the argyle class 
  public static void main(String[] args){ //open the main method of the program 
    Scanner S; //declare the scanner 
    S = new Scanner(System.in); //initalize the scanner with the constructor operator new 
    
    System.out.println("Welcome to the Argyle generator!"); //initial welcome 
    
    
    System.out.println("Please enter the window height:"); //prompt for window height 
    boolean height_test = S.hasNextInt(); //test for if it is an integer input 
    String junkWord; //used to wash out erroneous type inputs 
    
    while(height_test == false){ //test for the wrong type input of the height 
      junkWord = S.next(); //throws away junk word 
      System.out.println("Please enter an integer height:"); //re-prompt 
      height_test = S.hasNextInt();//waits for the user to input another integer
    }
    int height = S.nextInt(); //assigns the height variable 
    while(height <=0){
      System.out.println("Please enter a positive integer");
      height_test = S.hasNextInt();
      while(height_test == false){ //test for the wrong type input of the height 
        junkWord = S.next(); //throws away junk word 
        System.out.println("Please enter an integer height:"); //re-prompt 
        height_test = S.hasNextInt();//waits for the user to input another integer
    }
      height = S.nextInt();
    }
    System.out.println("Height: " + height); //print out to verify data to the user 
    
    
    //the statements below are exactly the same as for the height input 
    //they will find the window width 
    System.out.println("Please enter the window width as an integer:");
    
    boolean width_test = S.hasNextInt();
    while(width_test == false){
      junkWord = S.next();
      System.out.println("Please enter an integer width: ");
      width_test = S.hasNextInt();
    }
    int width = S.nextInt();
    while(width <=0){
      System.out.println("Please enter a positive integer");
      width_test = S.hasNextInt();
      while(width_test == false){ //test for the wrong type input of the width 
        junkWord = S.next(); //throws away junk word 
        System.out.println("Please enter an integer width:"); //re-prompt 
        width_test = S.hasNextInt();//waits for the user to input another integer
    }
      width = S.nextInt();
    }
    System.out.println("Width: "+ width );
    
    
    //these statements below are the same for the width and height
    //this will find the argyle side length with an integer value 
    System.out.println("Please enter the argyle width aka side length:");
    boolean A_width_test = S.hasNextInt(); 
    while(A_width_test == false){
      junkWord = S.next();
      System.out.println("Please enter an integer argyle width: ");
      A_width_test = S.hasNextInt();
    }
    int A_width = S.nextInt();
    while(A_width <=0){
      System.out.println("Please enter a positive integer");
      A_width_test = S.hasNextInt();
      while(A_width_test == false){ //test for the wrong type input of the height 
        junkWord = S.next(); //throws away junk word 
        System.out.println("Please enter an integer argyle width:"); //re-prompt 
        A_width_test = S.hasNextInt();//waits for the user to input another integer
    }
      A_width = S.nextInt();
    }
    System.out.println("Argyle Width aka diamond side length: "+ A_width );
    
    
    //these statements below will find the argyle center width
    //it is different than the statements above 
    System.out.println("Please enter the argyle center strip width as an odd integer :"); //input must be odd 
    System.out.println("It must be smaller than half of the diamond width"); //must be smaller than half the argyle side length for the geometry to work 
    boolean A_cent_width_test = S.hasNextInt();   //test center width to be an integer 
    while(A_cent_width_test == false){ //loop for getting rignt type of width 
      junkWord = S.next();
      System.out.println("Please enter an integer argyle center strip width : ");
      A_cent_width_test = S.hasNextInt();
    }
    int A_cent_width = S.nextInt();
    while(A_cent_width % 2 ==0 || A_cent_width > (A_width)/2.0 || A_cent_width<=0){ //test to make sure that the input is odd and not greater than half of the argyle width 
      System.out.println("Please enter an ODD  positive integer that is less than half the diamond width"); //prompt due to it being an integer and/or greater than half the argyle side length 
      A_cent_width_test = S.hasNextInt(); //variable type test 
         while(A_cent_width_test == false){ //test for the correct type within this statement 
           junkWord = S.next();
           System.out.println("Please enter an integer argyle center strip width : ");
           A_cent_width_test = S.hasNextInt();
     }
      A_cent_width = S.nextInt(); //final variable assignment 
    }
    System.out.println("Argyle center strip width: "+ A_cent_width ); 
    
    
    //these statements below will prompt the user for a character input three times and will only take the first character if any sort of string is entered 
    System.out.println("Please enter the first character type to go in the pattern fill");
    System.out.println("If you enter more than one letter it will only accept the first");
    //no test loop because all values will be acceptable strings
    String word1 = S.next();
    char type1 = word1.charAt(0); //will get the character in the 0th position
    System.out.println("type one: "+ type1); //print out for the user 
    
    System.out.println("Please enter the second character type to go in the pattern fill");
    System.out.println("If you enter more than one letter it will only accept the first");
    //no test loop because all values will be acceptable strings
    String word2 = S.next();
    char type2 = word2.charAt(0); 
    System.out.println("type two: "+ type2);
    
    System.out.println("Please enter the third character type to fill the diamond width");
    System.out.println("If you enter more than one letter it will only accept the first");
    //no test loop because all values will be acceptabel strings
    String word3 = S.next();
    char type3 = word3.charAt(0); 
    System.out.println("strip width type: "+ type3);
    
    
    
    
    
    //the variable initalizations and declarations below are essential to the proper functioning of the program
    //they may not be the most elegant solution but they worked fairly well in the solving of this problem but I am aware that a better solution exists
    
    int x_pos; //used to show the 'x' position between 0 and two times the argyle width 
    int y_pos; //used to show the 'y' position between 0 and two times the argyle width 
    double t = 1.0; //variable for middle section 
    int m =0; //variable used for the middle intersection of the left and right sides of the 'X' when the center width is greater than 1
    int max = 1; //used for at the middle where the right and left legs come together in the respective for loop
    int p = 0; //used to see if the middle interesection for that line has been evaluated 
    int u = 0; //will be used for the bottom left and bottom right where bottom left will actually be on the 'right' and bottom right will be on the 'left' in reference to the X formed 

    for(int i =1; i <= height; i++){//this is the outer most for loop that will move to the next line 
      y_pos = i%(2*A_width); //this is a very important value that will give the program the y positon from 0 to 2*A_width
      
      if(y_pos > 2*A_width - (int)(A_cent_width/2)||y_pos==0){ //this will increment the value used for the botton left and right parts in accordance with how the u value must change
        //to produce the correct print out 
        u++;
        
      }
      if(y_pos == 1){ //will work to reset the u value for the propoer print out 
        u=0;
     
      }
      
      
      for(int j = 1; j<=width; j++){ //print out statement for each line 
        x_pos = j%(2*A_width); //improtant value that gives the position in the x direction from 0 to two times A_width
        //these statements will print out the inner diamonds with type 2
        
        if(x_pos ==1){ // will be used to reset the value of p that is used for the middle intersection when reaching the next section once x_pos is 1
          p=0;
        }
        //this statement will produce the proper printout for the very middle of the intersection of the left and right arms of the formed X
        if(A_cent_width>1 && y_pos >=A_width && y_pos<=A_width+1&& x_pos ==A_width - (int)(A_cent_width/2)){ //constraints will assure the correct x value and y range 
          for(m =1; m<= A_cent_width&& j<=width; m++){ //this will print out the very middle part the desired number of times 
                System.out.print(type3);
                j++; //you need to increment j since this will produce multiple outputs and thus if you do not do this your print out will fall behind 
               }
              j--; //you have to do one decrement after to not add too many to j because you will increment it again when it goes back to the top of the loop 
          continue;//continue statements are used at pretty much all of the ends of the print out statements so that only the actual statement that needs to be printed will be
        }
        
        
        //left bottom which will appear on the right when y_pos goes to 0
        if(y_pos ==0 && x_pos == 2*A_width-(A_cent_width+1)/2+1){
          for(int d =1; d<=A_cent_width+1 && j<=width; d++){
            
            System.out.print(type3);
            j++;
          }
          j--;
          continue;
        }
        
        
        //this will print out the right number given y =0 and x=1 it was hard to handle within other cases so I gave it its own statement 
        if(y_pos==0 && x_pos==1 && A_cent_width>1){
         for(int g=1; g<=(int)(A_cent_width/2)+1 && j<=width;g++){
           
           System.out.print(type3);
           j++;
         }
          j--;
          continue;
       }
        
        
        
        
        //this statement will evaualte for the middle intersection above and below the very middle where very middle references the middle two rows that have the same number of type3 print outs 
        if(A_cent_width >1 && y_pos >= A_width - (int) (A_cent_width/2) + 1 && y_pos <= A_width - (int) (A_cent_width/2) +A_cent_width - 1 && x_pos>=A_width-(int) (A_cent_width/2) && p==0){
          if(y_pos <=A_width){
            if(y_pos == A_width - 1 || y_pos == A_width){ //these two if statements test to see where y_pos falls to give it the correct value for the print out to print the correct number 
              max = A_cent_width +1;
            }
            if(y_pos != A_width -1 || y_pos != A_width){
              t = (y_pos - A_width + (int) (A_cent_width/2))+A_cent_width/2 +.5;
              max =2*A_cent_width- (int)(2*t);
            }
              for(m =1; m<= max && j<=width; m++){ // j<= width is used so that in this print out loop it does not print out more than what the width sohuld be by being stuck inside of the for 
                //loop without knowing that it has gone too far 
                System.out.print(type3);
                j++;
               }
              j--;
              p++; //important that this is used for center evaluation 
              continue;
          }
          
          
          if(y_pos >A_width){ //will be exactly the same as above but now for the lower part of the middle intersection 
            if(y_pos == A_width +1){
              max = A_cent_width +1;
            }
            if(y_pos != A_width -1 || y_pos != A_width){
              t = (A_width - (int)(A_cent_width/2)+A_cent_width - y_pos) +A_cent_width/2 +.5;
              max =2*A_cent_width-(int)(2*t);
            }
              for(m =1; m<= max && j<=width; m++){
                    
                System.out.print(type3);
                j++;
               }
              j--;
            p++;
              continue;
          }
          continue;
        }
        
        
        //Bottom left will be on the 'right' print out 
        if(A_cent_width>1 && y_pos >=2*A_width-(int) (A_cent_width/2) + 1 && x_pos >= y_pos - A_cent_width+ (int)(A_cent_width/2)+1 && x_pos >A_width){ //explaining how I got these output ranges 
          //would take a long time but basically they are patterns I observed and they make sure that the print out starts at the right x and y positions 
          for(int k = 1; k<= A_cent_width-u && j<=width;k++){
           
            System.out.print(type3);
            j++;
          }
          j--;
          continue;
        }
        
           
        //left top as in the very top of the left side which will be truncated by interesecting with the 'wall' that limits the nubmer that can be inputted  
        if(A_cent_width >1  && y_pos <= (A_cent_width+1)/2 && x_pos <= y_pos + (int) (A_cent_width/2) && p == 0){
          for(int l = 1; l <= y_pos + (int) (A_cent_width/2) && j<=width; l++){
            
            System.out.print(type3);
            j++;
          }
          j--;
         continue;
        }
        
        //right bottom which will be on the left which will have very similar constraints to the left top 
        if(A_cent_width >1  && y_pos >= 2*A_width - (int) (A_cent_width/2) && x_pos == 1){
          for(int r = 1; r <= A_cent_width - u && j<= width; r++){
            
            System.out.print(type3);
            j++;
          }
          
          j--;
         continue;
        }
        
        //left which means the general printout for the left arm with a center width greater than 1 
        if(A_cent_width > 1 && x_pos == y_pos-(int)(A_cent_width/2) && p==0){
          for(int k = 1; k <= A_cent_width && j<=width; k++){
            
            System.out.print(type3);
            j++;
          }
          j--;
        continue;    
        }
        
       //right top which will have almost exactly the same for loop as the left bottom 
       if(A_cent_width > 1 && x_pos == 2*A_width-y_pos - (int)(A_cent_width/2) + 1 && y_pos != A_width - (int) (A_cent_width/2) && y_pos <= (A_cent_width-1)/2 && p==0){
          for(int k = 1; k <= y_pos + (int) (A_cent_width/2) && j<=width; k++){
            
            System.out.print(type3);
            j++;
          }
         j--;
        continue;   
        } 
        
        
        //right which means the general printout for the right arm with a center width greater than 1 
        if(A_cent_width > 1 && x_pos == 2*A_width-(int)(A_cent_width/2) - y_pos + 1 && p ==0 ){
          for(int k = 1; k <= A_cent_width && j <=width; k++){
            
            System.out.print(type3);
            j++;
          }
          j--;
        continue;    
        }
        
        
        
        //general band width print out 
        //this is the printout basically used if the center width is 1 
        //it is put all the way down here becasue it would be evaluated to true for most of the arm cases even with A_cent_width>1 which is not what we want 
        //so I put it down here below all of the previous statements 
        if(x_pos == y_pos || x_pos == 2*A_width-y_pos+1 ){
            if(p==0){
              System.out.print(type3);
              continue;
            }
        }
        
        
        //the finnishing printouts are for the type2 and type1 values that get printed out 
        //they will look for the cases that are type2 and if none are found it will print out a type1 value 
        if(y_pos== A_width+1 && x_pos ==0 && (j/A_width*10)%10 == 0){ //will check for the correct part of where x_pos = 0
          System.out.print(type2);
          continue;
        }
        if(y_pos >A_width && Math.abs(x_pos - A_width - .5) < A_width - y_pos%A_width+1){
          System.out.print(type2);
          continue;
        }
        
        
        if (Math.abs(x_pos - A_width - .5) < y_pos - 1 && y_pos<=A_width+1){
          System.out.print(type2);
          continue;
        }
        else if (x_pos ==0 && y_pos == 0 && A_cent_width == 1){ //special case for position x = 0  y = 0 
          System.out.print(type3);
          continue;
        }
        else if (x_pos ==0  && y_pos ==1 && A_cent_width == 1){ //special case x = 0 y = 1
          System.out.print(type3);
          continue;
        }
        else if (x_pos ==1  && y_pos ==0 && A_cent_width ==1){ //special case x = 1 y = 1
          System.out.print(type3);
          continue;
        }
        else if( i%(2*A_width) == 0 && Math.abs(x_pos - A_width - .5) < 1){
          System.out.print(type2);
          continue;
        }
        else{ //this is the final statement that will simply print out type1 if nothing else evaluated to true above 
          System.out.print(type1);
        }
        
      } //end of x position loop
      System.out.println("");//necessary void character with moving to the next line 
    }//end of y position loop 
    
    
  } //end of main method of program 
} //end of Argyle class of the whole program 