//CSE 2 - 210 Seth Slavin
//lab03 Check.java program
// 2-9-2018
//This program will prompt the user for the cost of a bill, the tip, and the number of people and will output how much each person should pay 
import java.util.Scanner; //import statement for the scanner 
public class Check{ //the start of the class 
  public static void main(String[] args){ //the start of the main fucntion where the code will lie 
    Scanner firstScanner; //declare the name of the scanner
    firstScanner = new Scanner(System.in); //call of the STDIN scanner with the constructor 'new'
    System.out.println("Enter the original cost of the check in the form xx.xx: ");//the first statement that will prompt the user for the total check 
    double checkCost = firstScanner.nextDouble();//calling the scanner method from the imported library for a double input 
    System.out.println("Enter the percentage tip that you wish to pay as a whole number (in the form xx):  ");//output prompting the user for the tip percentage
    double tipPercent = firstScanner.nextDouble();//calling the scanner method from the imported library for a double input
    System.out.println("Enter the number of people who went out to dinner: ");//output prompting the user for the number of people at dinner 
    int peopleCount = firstScanner.nextInt();//calling the scanner method from the imported library for an int input
    tipPercent/=100; //necessary to convert the tipPercent to a decimal
    double totalCost; //declare totalCost variable 
    double CostPerPerson; //declare CostPerPerson varaible 
    int dollars, dimes, pennies; //delcare the differnet units used for making the final price appear 
    
    totalCost = checkCost*(1+tipPercent); //calculation for the totalcost
    CostPerPerson = totalCost/peopleCount; //calculation for the cost per person 
    dollars = (int) CostPerPerson;//assign the cost per person to the dollars calue 
    dimes=(int)(CostPerPerson * 10) % 10;//assign the cost per person for the number of dimes 
    pennies=(int)(CostPerPerson * 100) % 10;//assign the cost per person for the number of pennies 
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //the final output in a standard monetary format 




  }
}