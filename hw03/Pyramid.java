//CSE 2 SP 18 Seth Slavin
//hw03 Pyramid.java program
//This program will take the inputs of the dimensions of a pyramid and output the volume of it
import java.util.Scanner; //import statement for the Scanner method for inputs 
public class Pyramid{ //start of the Pyramid Class 
  public static void main(String[] args){ //start of the main method of the program
    Scanner firstScanner; //declaration of the Scanner to be used 
    firstScanner = new Scanner(System.in); //the Scanner method is called with the constructor operator 'new'
    System.out.println("Please enter the side length of the base of the square pyramid: "); //prompt statement for the side length of the pyramid 
    double side = firstScanner.nextDouble(); //calling the scanner method from the imported library to assign an inputted double value to the variable side 
    System.out.println("Please enter the height of the pyramid from the base to the peak: "); //prompt statement for the height of the pyramid 
    double height = firstScanner.nextDouble(); //calling the scanner method for a double input to assign a double value to the variable height 
    double volume = side*side*height/3; //the calculation for the volume of a square base pyramid which is the square of the side times the height divided by 3
    System.out.println("The volume of the pyramid is " + volume + " cubic units "); //the final display statement of the program for the user to see the volume 
  }//end of the main method of the program 
}//end of the Pyramid class