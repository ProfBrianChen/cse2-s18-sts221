//CSE2-SP18 Seth Slavin
//hw03
//this program will take input from a user about the amount of
//rainfall due to a hurricane over a value of land representing
//acres and then the number of inches that fell and
//this will then be used to convert to cubic miles
import java.util.Scanner; //the import command for the scanner method
public class Convert { //beginning of the class Convert
  public static void main(String[] args ){ //beginning of the main method of the program
    Scanner firstScanner; //declaration of the Scanner 
    firstScanner = new Scanner(System.in); //'call' the scanner firstScanner with the constructor operator 'new'
    System.out.println("Please enter the number of affected acres in a decimal format (double): "); //prompt statement for the area affected 
    double acres = firstScanner.nextDouble(); //calling the scanner method from the scanner library for a double input from the SSH terminal
    System.out.println("Please enter the number of inches of rain fall as a decimal (double): "); //prompt for the number of inches of rainfall
    double inches = firstScanner.nextDouble(); //calling the scanner method from the scanner library for a double input from the SSH terminal 
    double volume = acres* inches *2.466e-8; //calculation for the volume, the conversion factor of 2.466e-8 comes from the number of square 
    //feet in an acre 43560 divided by 12 inches per foot divided by the cube of 5280 feet per mile, it was written just as this number to 
    //eliminate user confusion 
    System.out.println("the volume of water that fell in cubic miles is " + volume); //the final print out statement for the volume in cubic miles  
  } //end bracket for the main method 
} //end bracket for the class Convert 