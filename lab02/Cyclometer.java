//CSE2 210 fundamentals of computer programming 
//Seth Slavin
//1_2_18 lab02



public class Cyclometer{
  //this is the main method of the program
  public static void main(String[] args){
    //input data
    int secsTrip1=480;  //time for trip 1
    int secsTrip2=3220;  //time for trip 2
		int countsTrip1=1561;  //rotations for trip 1
		int countsTrip2=9037; //rotations for trip 2
    //intermediate varaibles and output data
    double wheelDiameter=27.0,  //diameter of the wheel
  	PI=3.14159, //approximation of pi
  	feetPerMile=5280,  //feet in a mile 
  	inchesPerFoot=12,   //inches in a foot
  	secondsPerMinute=60;  //secodns in a minute
	  double distanceTrip1, distanceTrip2, totalDistance; //output variable declarations 
    //the follwoing statemetns will print out the numbers stored in varaibles 
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");
		//calculations for distance
		distanceTrip1=countsTrip1*wheelDiameter*PI/inchesPerFoot/feetPerMile; // Gives distance in miles
		distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//gives distance in miles
		totalDistance=distanceTrip1+distanceTrip2;//total distance calculation 
		//we will now print out the distances 
		System.out.println("Trip 1 was "+distanceTrip1+" miles");//print out for trip 1
		System.out.println("Trip 2 was "+distanceTrip2+" miles");//print out for trip2 
		System.out.println("The total distance was "+totalDistance+" miles");//print out for the two trips 

  }//end of main method
} //end of class