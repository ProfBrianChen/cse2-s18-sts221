//cse2 hw01 Welcome Class Script        
//Seth Slavin 

public class WelcomeClass {
  public static void main (String[] args) {
    //the below statements will print out the desired information for the homework
    //these are the welcome statemnet 
    System.out.println("      -----------");
    System.out.println("      | WELCOME |");
    System.out.println("      -----------");
    //these will print out the lehigh email
    System.out.println("  ^   ^   ^   ^   ^   ^");
    System.out.println(" / \\ / \\ / \\ / \\ / \\ / \\ ");
    System.out.println("<-S --T --S --2 --2 --1->");
    System.out.println(" \\ / \\ / \\ / \\ / \\ / \\ /");
    System.out.println("  v   v   v   v   v   v");
    //these final ones will print my biographical tweet
    System.out.println("I am Seth Slavin, a freshman");
    System.out.println("at Lehigh University studying");
    System.out.println("electrical engineering and possibly");
    System.out.println("computer sciences. I am also on the");
    System.out.println("track and cross country teams.");
  }
}