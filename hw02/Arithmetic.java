//CSE2 Fundamentals of Programming
//Seth Slavin SP18 hw02
//this program will essentially act as an Arithmetic Calculator for finding the total cost of a store bill with taxes and various items 

public class Arithmetic{
  public static void main(String[] args){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //cost per box of envelopes
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    //intermediate variable declarations
    //these three are the product of the number of each item by the price per item
    double totalCostofPants;
    double totalCostofShirts;
    double totalCostofBelts;
    //these will be the values of the sales tax charged by buying all of each kind of item
    double salesTaxTotalPants;
    double salesTaxTotalShirts;
    double salesTaxTotalBelts;
    //total cost of purchases before sales tax 
    double totalCostBeforeTax;
    //total amount of sales tax 
    double totalCostofSalesTax;
    //the total cost after sales tax is calculated
    double totalCostAfterTax;
    //the following statments will act as variable assignments for the varaibles declared above
    //these statements will find the product of the number of items and total cost for each of the three items purcahsed
    totalCostofPants = numPants*pantsPrice;
    totalCostofShirts = numShirts*shirtPrice;
    totalCostofBelts = numBelts*beltCost;
    // the statemetns below will find the total of all of these items before sales tax is applied
    totalCostBeforeTax = totalCostofPants + totalCostofShirts + totalCostofBelts;
    //the statements below will find the amount of tax per total of each item
    salesTaxTotalPants = totalCostofPants*paSalesTax;
    salesTaxTotalShirts = totalCostofShirts*paSalesTax;
    salesTaxTotalBelts = totalCostofBelts*paSalesTax;
    //the statement below will find the tax total for all the items
    totalCostofSalesTax = salesTaxTotalBelts + salesTaxTotalShirts + salesTaxTotalPants;
    //the below statment will find the total cost after tax 
    totalCostAfterTax = totalCostBeforeTax + totalCostofSalesTax;
    
    //the following statements will display the all of the values calcualted above
    //the statement below will display the total cost per item before tax starting with Pants
    System.out.println("Price for all pants before tax: $" + (totalCostofPants));
    //the next one will display the total cost of shirts before sales tax
    System.out.println("Price for all sweatshirts before tax: $" + (totalCostofShirts));
    //the next one will display the total cost of belts before sales tax
    System.out.println("Price for all belts before tax: $" + (totalCostofBelts));
    //the following statements will print out the sales tax totals for buying all of each item 
    //the following line displays the total sales tax paid for the pants
    //The String command in the output is used to assure that there will be only two decimal places 
    System.out.println("Sales tax total for all pants: $" + String.format("%.2f", salesTaxTotalPants));
    //the following line displays the total sales tax paid for the shirts
    System.out.println("Sales tax total for all sweatshirts: $" + String.format("%.2f", salesTaxTotalShirts));
    //the following line displays the total sales tax paid for the belts
    System.out.println("Sales tax total for all belts: $" + String.format("%.2f", salesTaxTotalBelts));
    //the statment below will display the total cost before sales tax
    System.out.println("Total price before sales tax : $" + (totalCostBeforeTax));
    //the statement below will displau the total amount of sales tax
    System.out.println("Total amount of Sales Tax: $" + String.format("%.2f", totalCostofSalesTax));
    //the statement below will print the final price with sales tax
    System.out.println("Final price after sales tax: $" + String.format("%.2f", totalCostAfterTax));
  }
}