//CSE2-SP18 Seth Slavin 
//Professor Chen
//hw04 Yahtzee.java program
//purpose: This lengthy program runs a simulation of the game Yahtzee. This program allows the user to either input values on their own or choose to have them 
//randomly generated. These values are then interpreted by all of the statments below, in accordance with the rules of Yahtzee, to produce the decision the user would
//make. If all dice are the same you get a Yahtzee, if they are all in order you got a large straight, if four of the five are in order you get a small striahgt, if
//four of them are the same you got four of a kind, if three are the same you got three of a kind, if you got three of a kind and a pair you will have a full house, 
//and the final "chance" will simply sum the dice up. This is how the lower section is scored. The upper section can be scored at the same time, but basically the 
//rule is to look for the same number on the dice and sum that up. During each turn a box must be filled, so if no new boxes are checked off the program will fill
//in zero for the least valued box that is yet to be filled. With 13 boxes the program will run through 13 times and then output the final score in accordance with 
//the lower, upper, and total scoring rules 



import java.util.Scanner; //import the class for the scanner 
public class Yahtzee{ //start of the class Yahtzee 
  
  public static void main(String[] args){ //start of the main method of the program
    
    Scanner firstScanner; // declare the scanner 
    firstScanner = new Scanner(System.in); 
    
    System.out.println("WELCOME TO YAHTZEE!!!"); //first time run through screen
    System.out.println("_____________________");
    String yes = "Yes"; //declaration and initialization placeholder 
    String no = "No";//declaration and initialization placeholder  
    
    int num; //this references the actual number that the user inputs 
    
    int score = 0; //this represents the upper score, I wanted to make it 'Uscore' but by the time I got to working on the lower score I had used it so many times 
    int Lscore = 0; //lower score value 
    int small_st = 0; //state of the small straight where 0 means it still was not filled and 1 means it was 
    int large_st = 0; //state of the large straight 
    int five_kind =0; //state of the five_kind aka Yahtzee slot 
    
    int t=0; //this is the most important variable for the program. In the while loop, once a box has been filled this will be incremented and will not fill any 
    //other boxes. It is initialized back to 1 at the begginning of the loop
    
    int p =0; //this is also a very important variable. It can get messy with how three of something is three of a kind or a score in the upper section. This varaible
    //is for the three of a kind and once it turns to a 1 it will no longer read three of the same as three of a kind and will score it in the upper section 
  
    int full = 0; //state of the full house box
    int p_4 = 0;  //same as for 'p' but for four of a kind 
    
    //Some of the variables may not be the best solution but they are what I built the program on and taking them out of 500+ lines would have taken much more time 
    int num_1, num_2, num_3, num_4, num_5, num_6; //declaration of the 'num' varaibles. These represent the number of each of 1,2,3,4,5,6 that appear in a given roll
    num_1 = num_2 = num_3 = num_4 = num_5 = num_6 = 0; // the state of the 'num' variables is initialized to 0
    int dice_1,dice_2,dice_3,dice_4,dice_5; //declaration of the dice that will carry either the values inputted or randomly generated dice
    int box_1, box_2, box_3, box_4, box_5, box_6, box_7, box_8, box_9, box_10, box_11, box_12, box_13; //these represent the boxes used in the scorecard provided that
    //goes from 1 at the top being the 'ones' to 13 being 'chance'
    
    box_1 = box_2 = box_3 = box_4 = box_5 = box_6 = box_7 = box_8 = box_9 = box_10 = box_11 = box_12 = box_13 = 0;//when the box has a value of 0 it means that it is
    //empty and when it is set to 1 it is filled 
    
    while (box_1 == 0 || box_2 == 0 || box_3 ==0 || box_4 == 0 || box_5 == 0 || box_6==0 || box_7 ==0 || box_8 ==0 || box_9 ==0 || box_10 ==0 || box_11 ==0 || box_12 ==0 || box_13 ==0){
       //I know we did not learn the while loop yet but I did not see any other reasonable way to do this. The code will be executed 13 times no matter what is 
       //randomly generated or input but simply rerunning through the same code without a loop would mean I would have to copy it 13 times. For about 500 lines, over 
       //6000 lines is not desirable so I did this.

        t=0; //this is the check to see if a box has been filled. We reset it to 0 at the begginning of each new loop
        num_1 = num_2 = num_3 = num_4 = num_5 = num_6 = 0; //these must be reset to 0
        System.out.println("Would you like to input the dice values or have them randomly rolled?"); // repeated prompts for user input 
        System.out.println("Type in 'Yes' to input them and 'No' to have them generated for you.");

        String decision = firstScanner.next(); //calling the scanner method 

      if(yes.equals(decision)){ //code if 'Yes' is properly input 
        System.out.println("Please enter the five digits unbroken: ");
        num = firstScanner.nextInt();
        //the next few lines below will take the 5 digit number and turn it into 5 individual dice values 
        dice_1 = num%10;
        dice_2 =((num%100)-dice_1)/10;
        dice_3 = ((num%1000)-dice_2*10-dice_1)/100;
        dice_4 = ((num%10000)-dice_3*100-dice_2*10-dice_1)/1000;
        dice_5 = ((num%100000)-dice_4*1000-dice_3*100-dice_2*10-dice_1)/10000;

        if( num <11111){ //the following statements check to see if the input was erroneous or illogical and if any part of them is then it will just generate them
          System.out.println("Erroneous input; we will generate them for you: ");
          dice_1= (int)((Math.random()*6)+1); //this statement is used to output random numbers to simulate a dice roll between 1 and 6 
          dice_2= (int)((Math.random()*6)+1);
          dice_3= (int)((Math.random()*6)+1);
          dice_4= (int)((Math.random()*6)+1);
          dice_5= (int)((Math.random()*6)+1);
        }
        if(num > 66666){
          System.out.println("Erroneous input; we will generate them for you: ");
          dice_1= (int)((Math.random()*6)+1);
          dice_2= (int)((Math.random()*6)+1);
          dice_3= (int)((Math.random()*6)+1);
          dice_4= (int)((Math.random()*6)+1);
          dice_5= (int)((Math.random()*6)+1);
        }
        if (dice_1 <1 || dice_1>6){ //these next 6 statemetns check to see if the value of each die is between 1 and 6 
          System.out.println("Erroneous input; we will generate them for you: ");
          dice_1= (int)((Math.random()*6)+1);
          dice_2= (int)((Math.random()*6)+1);
          dice_3= (int)((Math.random()*6)+1);
          dice_4= (int)((Math.random()*6)+1);
          dice_5= (int)((Math.random()*6)+1);
        }
        if (dice_2 <1 || dice_2>6){
          System.out.println("Erroneous input; we will generate them for you: ");
          dice_1= (int)((Math.random()*6)+1);
          dice_2= (int)((Math.random()*6)+1);
          dice_3= (int)((Math.random()*6)+1);
          dice_4= (int)((Math.random()*6)+1);
          dice_5= (int)((Math.random()*6)+1);
        }
        if (dice_3 <1 || dice_3>6){
          System.out.println("Erroneous input; we will generate them for you: ");
          dice_1= (int)((Math.random()*6)+1);
          dice_2= (int)((Math.random()*6)+1);
          dice_3= (int)((Math.random()*6)+1);
          dice_4= (int)((Math.random()*6)+1);
          dice_5= (int)((Math.random()*6)+1);
        }
        if (dice_4 <1 || dice_4>6){
          System.out.println("Erroneous input; we will generate them for you: ");
          dice_1= (int)((Math.random()*6)+1);
          dice_2= (int)((Math.random()*6)+1);
          dice_3= (int)((Math.random()*6)+1);
          dice_4= (int)((Math.random()*6)+1);
          dice_5= (int)((Math.random()*6)+1);
        }
        if (dice_5 <1 || dice_5>6){
          System.out.println("Erroneous input; we will generate them for you: ");
          dice_1= (int)((Math.random()*6)+1);
          dice_2= (int)((Math.random()*6)+1);
          dice_3= (int)((Math.random()*6)+1);
          dice_4= (int)((Math.random()*6)+1);
          dice_5= (int)((Math.random()*6)+1);
        }


      }
      else if (no.equals(decision)){ //random generation for 'No' response 
        dice_1= (int)((Math.random()*6)+1);
        dice_2= (int)((Math.random()*6)+1);
        dice_3= (int)((Math.random()*6)+1);
        dice_4= (int)((Math.random()*6)+1);
        dice_5= (int)((Math.random()*6)+1);

      }
      else{ //this will assure that no matter what is put in that it will generate the dice randomly 
        System.out.println("Erroneous input; we will generate them for you: ");
        dice_1= (int)((Math.random()*6)+1);
        dice_2= (int)((Math.random()*6)+1);
        dice_3= (int)((Math.random()*6)+1);
        dice_4= (int)((Math.random()*6)+1);
        dice_5= (int)((Math.random()*6)+1);
      }
     //these statements below will output the dice values 
     System.out.println("Dice 1: " + dice_1);    
     System.out.println("Dice 2: " + dice_2);
     System.out.println("Dice 3: " + dice_3);
     System.out.println("Dice 4: " + dice_4);
     System.out.println("Dice 5: " + dice_5);

      
     //the switch statements below will find the number of each of the 6 possible values that appear for all five dice 
      switch(dice_1){
        case 1:
          num_1++;
          break;
        case 2:
          num_2++;
          break;
        case 3:
          num_3++;
          break;
        case 4:
          num_4++;
          break;
        case 5:
          num_5++;
          break;
        case 6:
          num_6++;
          break;
      }
      switch(dice_2){
        case 1:
          num_1++;
          break;
        case 2:
          num_2++;
          break;
        case 3:
          num_3++;
          break;
        case 4:
          num_4++;
          break;
        case 5:
          num_5++;
          break;
        case 6:
          num_6++;
          break;
      }
      switch(dice_3){
        case 1:
          num_1++;
          break;
        case 2:
          num_2++;
          break;
        case 3:
          num_3++;
          break;
        case 4:
          num_4++;
          break;
        case 5:
          num_5++;
          break;
        case 6:
          num_6++;
          break;
      }
     switch(dice_4){
        case 1:
          num_1++;
          break;
        case 2:
          num_2++;
          break;
        case 3:
          num_3++;
          break;
        case 4:
          num_4++;
          break;
        case 5:
          num_5++;
          break;
        case 6:
          num_6++;
          break;
      }
      switch(dice_5){
        case 1:
          num_1++;
          break;
        case 2:
          num_2++;
          break;
        case 3:
          num_3++;
          break;
        case 4:
          num_4++;
          break;
        case 5:
          num_5++;
          break;
        case 6:
          num_6++;
          break;
      }
            
      
      //the statement bellow will be executed for the last "Chance" box once all other boxes have been filled 
     if (box_13 ==0 && box_1 == 1 && box_2 == 1 && box_3 == 1 && box_4 == 1 && box_5 == 1 && box_6 == 1 && box_7 ==1 && box_8 ==1 && box_9 == 1 && box_10 ==1 && box_11 == 1 && box_12==1){
       System.out.println("Chance!");
       Lscore = Lscore + (dice_1 + dice_2 + dice_3 + dice_4 + dice_5);
       box_13 = 1;
       t++;
     }
      
     
     //the statemetn below will check to see if all of the dice are equal and thus if there is a YAHTZEE
     if(dice_1 == dice_2 && dice_2 == dice_3 && dice_3 == dice_4 && dice_4 == dice_5){
       if(five_kind ==0){       
        System.out.println("YAHTZEE");
         Lscore = Lscore + 50;
         t++;
         box_12 =1;
         five_kind=1;
       }
     }      
      
      
     //The following two statements will test for the two possbilities for a large straight. I know we are not supposed to nest this many if statements, but I was 
     //not sure what else could be done since it is a somewhat difficult evaluation with the values not being inputted or generated in necessarily any order 
     if(large_st == 0){
      if(dice_1 == 1 || dice_2 == 1 || dice_3 == 1 || dice_4 == 1 || dice_5 == 1) {
        if(dice_1 == 2 || dice_2 == 2 || dice_3 == 2 || dice_4 == 2 || dice_5 == 2){
          if(dice_1 == 3 || dice_2 == 3 || dice_3 == 3 || dice_4 == 3 || dice_5 == 3){
            if(dice_1 == 4 || dice_2 == 4 || dice_3 == 4 || dice_4 == 4 || dice_5 == 4){
              if(dice_1 == 5 || dice_2 == 5 || dice_3 == 5 || dice_4 == 5 || dice_5 == 5){          
                System.out.println("1-2-3-4-5 large Straight! ");
                Lscore = Lscore + 40;
                t++;
                large_st++;
                box_11 =1;
              }
            }
          }
        }
      }
     }
     if(large_st == 0){
      if(dice_1 == 2 || dice_2 == 2 || dice_3 == 2 || dice_4 == 2 || dice_5 == 2) {
        if(dice_1 == 3 || dice_2 == 3|| dice_3 == 3 || dice_4 == 3 || dice_5 == 3){
          if(dice_1 == 4 || dice_2 == 4 || dice_3 == 4 || dice_4 == 4 || dice_5 == 4){
            if(dice_1 == 5 || dice_2 == 5 || dice_3 == 5 || dice_4 == 5 || dice_5 == 5){
              if(dice_1 == 6 || dice_2 == 6 || dice_3 == 6 || dice_4 == 6 || dice_5 == 6){          
               System.out.println("2-3-4-5-6 Large Straight! ");
               Lscore = Lscore + 40;
               t++;
               large_st++;
               box_11 =1;
              }
            }
          }
        }
      }
     }   
       
      
     //The following three blocks of code will test if there is a small straight with the three possible combinations just like for the large straight
     if(small_st == 0 && t==0){
      if(dice_1 == 1 || dice_2 == 1 || dice_3 == 1 || dice_4 == 1 || dice_5 == 1) {
        if(dice_1 == 2 || dice_2 == 2 || dice_3 == 2 || dice_4 == 2 || dice_5 == 2){
          if(dice_1 == 3 || dice_2 == 3 || dice_3 == 3 || dice_4 == 3 || dice_5 == 3){
            if(dice_1 == 4 || dice_2 == 4 || dice_3 == 4 || dice_4 == 4 || dice_5 == 4){

              System.out.println("1-2-3-4 Small Straight! ");
              Lscore = Lscore + 30;
              t++;
              small_st++;
              box_10 =1;
            }
          }
        }
      }
     }
     if(small_st == 0 && t==0){
      if(dice_1 == 2 || dice_2 == 2 || dice_3 == 2 || dice_4 == 2 || dice_5 == 2) {
        if(dice_1 == 3 || dice_2 == 3 || dice_3 == 3 || dice_4 == 3 || dice_5 == 3){
          if(dice_1 == 4 || dice_2 == 4 || dice_3 == 4 || dice_4 == 4 || dice_5 == 4){
            if(dice_1 == 5 || dice_2 == 5 || dice_3 == 5 || dice_4 == 5 || dice_5 == 5){

              System.out.println("2-3-4-5 Small Straight! ");
              Lscore = Lscore + 30;
              t++;
              small_st++;
              box_10 = 1;
            }
          }
        }
      }
     }
     if(small_st == 0 && t==0){
      if(dice_1 == 3 || dice_2 == 3 || dice_3 == 3 || dice_4 == 3 || dice_5 == 3) {
        if(dice_1 == 4 || dice_2 == 4 || dice_3 == 4 || dice_4 == 4 || dice_5 == 4){
          if(dice_1 == 5 || dice_2 == 5 || dice_3 == 5 || dice_4 == 5 || dice_5 == 5){
            if(dice_1 == 6 || dice_2 == 6 || dice_3 == 6 || dice_4 == 6 || dice_5 == 6){

              System.out.println("3-4-5-6 Small Straight! ");
              Lscore = Lscore + 30;
              t++;
              small_st++;
              box_10 = 1;
            }
          }
        }
      }
     }   
      
      
     //box_8 is for four of a kind which is where we start having trouble with the precedence of the lower and upper scoring. Although in an actual game someone
     //may not choose to fill in the four of a kind box first, we will do it here 
     if( box_8 == 0){
      if( num_1 >=4 || num_2 >= 4 || num_3 >= 4 || num_4 >= 4 || num_5 >= 4 || num_6 >= 4){ //we must test for values greater than or equal to 4 because we could 
        //end up with 5 of something and if we already used YAHTZEE we must be able to score four of those dice
        if(num_6 >= 4 && p_4 ==0 && t==0){ 
            System.out.println("Four of a kind!!!");
            box_8= 1; //'fills' box number 8
            Lscore = Lscore + 24; //score increase for 4 dice with a value of 6 each 
            p_4++; //this checks off that four of a kind has been completed. This is essentially the same as checking off box number 8 
            t++;
          }
          if(num_5 >= 4 && p_4 ==0 && t==0){
            System.out.println("Four of a kind!!!");
            box_8= 1;
            Lscore = Lscore + 20;
            p_4++;
            t++;
          }
          if(num_4 >= 4 && p_4 ==0 && t ==0){
            System.out.println("Four of a kind!!!");
            box_8= 1;
            Lscore = Lscore + 16;
            p_4++;
            t++;
          }
          if(num_3 >= 4 && p_4 ==0 && t==0){
            System.out.println("Four of a kind!!!");
            box_8= 1;
            Lscore = Lscore + 12;
            p_4++;
            t++;
          }
          if(num_2 >= 4 && p_4 ==0 && t==0){
            System.out.println("Four of a kind!!!");
            box_8= 1;
            Lscore = Lscore + 8;
            p_4++;
            t++;
          }
          if(num_1 >= 4 && p_4 ==0 && t==0){
            System.out.println("Four of a kind!!!");
            box_8= 1;
            Lscore = Lscore + 4;
            p_4++;
            t++;
          }
        }
     }
       
       
       //these statements test for the upper score boxes 1 through 6 in the case that four of a kind has already been completed. This is where 't' being 1 prevents 
       //the score from counting 4 of something as both four of a kind and four of that value in the upper score section 
        if(num_1 >=4 && box_1 == 0 &&  t ==0){
          System.out.println("Four ones!");
          score = score+4;
          box_1 = 1;
          t++;
        }
       if(num_2 >=4 && box_2 == 0 && t==0){
          System.out.println("Four twos!");
          score = score+8;
          box_2 = 1;
          t++;
        }
       if(num_3 >=4 && box_3 == 0 && t==0){
         System.out.println("Four threes!");
         score = score+12;
         box_3 = 1;
         t++;
        }
      if(num_4 >=4 && box_4 ==0 && t==0){
        System.out.println("Four fours!");
        score = score+16;
        box_4 = 1;
        t++;
        }
      if(num_5 >=4 && box_5 == 0 && t==0){
        System.out.println("Four fives!");
        score = score+20;
        box_5 = 1;
        t++;
      }
     if(num_6 >=4 && box_6 ==0 && t==0){
        System.out.println("Four sixes!");
        score = score+24;
        box_6 = 1;
        t++;
      }
      
      
      //now these statements will work on arrangments of THREE
      if( num_1 >=3 || num_2 >= 3 || num_3 >= 3 || num_4 >= 3 || num_5 >= 3 || num_6 >=3){
          if( num_1 ==2 || num_2 == 2 || num_3 == 2|| num_4 == 2 || num_5 == 2 || num_6 ==2 ){ //this will first test if there is three of a kind and a pair which is
            //a full house
            if( full ==0){
              System.out.println("Full house!");
              Lscore = Lscore+25;
              box_9 =1;
              full++; //will make sure that only one full house will happen
              t++;
            }

          }
        
        if(p==0 && t ==0 && box_7 ==0){ //'p' will be incremented after the first three of a kind is chosen and thus after that all three of a kind will go into the
          //upper section score total
          if(num_6 >= 3 && p ==0){
            System.out.println("Three of a kind!");
            box_7= 1; //assures that the three of a kind box is filled 
            Lscore = Lscore + 18;
            p++;
            t++;
          }
          if(num_5 >= 3 && p ==0){
            System.out.println("Three of a kind!");
            box_7= 1;
            Lscore = Lscore + 15;
            p++;
            t++;
          }
          if(num_4 >= 3 && p ==0){
            System.out.println("Three of a kind!");
            box_7= 1;
            Lscore = Lscore + 12;          
            p++;
            t++;
          }
          if(num_3 >= 3 && p ==0){
            System.out.println("Three of a kind!");
            box_7= 1;
            Lscore = Lscore + 9;
            p++;
            t++;
          }
          if(num_2 >= 3 && p ==0){
            System.out.println("Three of a kind!");
            box_7= 1;
            Lscore = Lscore + 6;
            p++;
            t++;
          }
          if(num_1 >= 3 && p ==0){
            System.out.println("Three of a kind!");
            box_7= 1;
            Lscore = Lscore + 3;
            p++;
            t++;
          }
        }

        
         //these statements will work on the upper scoring. They go from 6 down to 1 because you would want to take the higher combination, but this does not really 
         //matter for sets of three becasue you can only have 1 in a set of 5 dice 
         if(num_6 >=3 && box_6 ==0 && t==0){
            System.out.println("Three sixes!");
            score = score+18;
            box_6 = 1;
            t++;
          }
          if(num_5 >=3 && box_5 == 0 && t==0){
            System.out.println("Three fives!");
            score = score+15;
            box_5 = 1;
            t++;
          }
          if(num_4 >=3 && box_4 ==0 && t==0){
            System.out.println("Three fours!");
            score = score+12;
            box_4 = 1;
            t++;
            }
         if(num_3 >=3 && box_3 == 0 && t==0){
             System.out.println("Three threes!");
             score = score+9;
             box_3 = 1;
             t++;
            } 
         if(num_2 >=3 && box_2 == 0 && t==0){
              System.out.println("Three twos!");
              score = score+6;
              box_2 = 1;
              t++;
            } 
         if(num_1 >=3 && box_1 == 0 &&  t ==0){
              System.out.println("Three ones!");
              score = score+3;
              box_1 = 1;
              t++;
            }
      }
      
      
      //we will now run similar code for pairs, two of a kind. 
      if( num_1 >=2 || num_2 >= 2 || num_3 >= 2|| num_4 >= 2 || num_5 >= 2 || num_6 >=2 && t==0){//we must check if the value is not just two but greater than two 
        
       if(num_6 >=2 && box_6 ==0 && t==0){
          System.out.println("Two sixes!");
          score = score+12;
          box_6 = 1;
          t++;
        }
       if(num_5 >=2 && box_5 == 0 && t==0){
          System.out.println("Two fives!");
          score = score+10;
          box_5 = 1;
          t++;
        }
       if(num_4 >=2 && box_4 ==0 && t==0){
         System.out.println("Two fours!");
         score = score+8;
         box_4 = 1;
         t++;
        } 
       if(num_3 >=2 && box_3 == 0 && t==0){
         System.out.println("Two threes!");
         score = score+6;
         box_3 = 1;
         t++;
        }
       if(num_2 >=2 && box_2 == 0 && t==0){
         System.out.println("Two twos!");
         score = score+4;
         box_2 = 1;
         t++;
        }
       if(num_1 >=2 && box_1 == 0  && t ==0){
         System.out.println("Two ones!");
         score = score+2;
         box_1 = 1;
         t++;
        }
      }
      
     //We will run very similar code for the case where it pretty much has to pick which of the values is the largest to give one of them in the upper boxes 
     if( num_1 >=1 || num_2 >= 1 || num_3 >= 1 || num_4 >= 1 || num_5 >= 1 || num_6 >=1 && t==0){
       
        if(num_6 >=1 && box_6 ==0 && t==0){
          System.out.println("One six!");
          score = score+6;
          box_6 = 1;
          t++;
        }
       if(num_5 >=1 && box_5 == 0 && t==0){
          System.out.println("One five!");
          score = score+5;
          box_5 = 1;
          t++;
        }
       if(num_4 >=1 && box_4 ==0 && t==0){
          System.out.println("One four!");
          score = score+4;
          box_4 = 1;
          t++;
        } 
       if(num_3 >=1 && box_3 == 0 && t==0){
          System.out.println("One three!");
          score = score+3;
          box_3 = 1;
          t++;
        }
       if(num_2 >=1 && box_2 == 0 && t==0){
          System.out.println("One two!");
          score = score+2;
          box_2 = 1;
          t++;
        }
       if(num_1 >=1 && box_1 == 0  && t ==0){
         System.out.println("One one!");
         score = score+1;
         box_1 = 1;
         t++;
        }
      }
      
      
      //this statement will check if none of the boxes were filled during the entire program. t will be zero becasue it would not have been incremented.
      //this is essential code as it assures that the program will not keep running. It will only have you roll or randomly 'roll' the dice 13 times 
      if( t==0 ){
        if(box_1 == 0 ){
          box_1 =1;
          System.out.println("Box 1 filled!"); //the box will be filled but there will be a zero contribution to the score 
          t++;
        }
        if(box_2 == 0 && t==0){
          box_2 =1;
          System.out.println("Box 2 filled!");
          t++;
        }
        if(box_3 == 0 && t==0){
          box_3 =1;
          System.out.println("Box 3 filled!");
          t++;
        }
        if(box_4 == 0 && t==0){
          box_4 =1;
          System.out.println("Box 4 filled!");
          t++;
        }
        if(box_5 == 0 && t==0){
          box_5 =1;
          System.out.println("Box 5 filled!");
          t++;
        }
        if(box_6 == 0 && t==0){
          box_6 =1;
          System.out.println("Box 6 filled!");
          t++;
        }
        if(box_7 == 0 && t==0){
          box_7 =1;
          System.out.println("Box 7 filled!");
          t++;
        }
        if(box_8 == 0 && t==0){
          box_8 =1;
          System.out.println("Box 8 filled!");
          t++;
        }
        if(box_9 == 0 && t==0){
          box_9 =1;
          System.out.println("Box 9 filled!");
          t++;
        }
        if(box_10 == 0 && t==0){
          box_10 =1;
          System.out.println("Box 10 filled!");
          t++;
        }
        if(box_11 == 0 && t==0){
          box_11 =1;
          System.out.println("Box 11 filled!");
          t++;
        }
        if(box_12 == 0 && t==0){
          box_12 =1;
          System.out.println("Box 12 filled!");
          t++;
        }
        if(box_13 == 0 && t==0){
          box_13 =1;
          System.out.println("Box 13 filled!");
          t++;
        }
      }  
      
      //these outputs will print after each dice roll throughout the program 
      System.out.println("Upper Score: " + score);
      System.out.println("Lower Score: " + Lscore);
    }
    
    
    System.out.println("Initial Upper Score: " + score); //this will print out the inital upper score before we consider the bonus 
    
    if( score >= 63){ //will determine if a bonus will happen, you will need at least three of each in the upper seciton 
      score = score +35;      
    }
    
    System.out.println("FINAL Upper Score: " + score); //final upper section total score output 
    System.out.println("FINAL Lower Score: " + Lscore); //final lower section total score output 
    int total = score + Lscore; //total score calculation 
    System.out.println("FINAL Total Score: " + total); //print out statement for the final total score 
  }
}